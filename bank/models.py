from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _


"""
model for store transaction between to user
1 - trnsaction from user,sent amount to antother user
2- transaction date store trnsaction date sent
2- transaction date store trnsaction date sent
"""
class Transactions(models.Model):
    STATUS = (
        ("SUCCESS", "success"),
        ("FAILED", "failed"),
    )
    transaction_from = models.ForeignKey(User, verbose_name=_("transaction from"),
                                         on_delete=models.CASCADE, related_name="t_from")
    transaction_date = models.DateTimeField(verbose_name=_("transaction date"), auto_now_add=True)
    amount = models.BigIntegerField(verbose_name=_("amount"), default=100)
    fee = models.BigIntegerField(verbose_name=_("feee"), default=10)
    transaction_to = models.ForeignKey(User, verbose_name=_("transaction to"),
                                       on_delete=models.CASCADE, related_name="t_to")
    state = models.CharField(verbose_name=_("state"), max_length=10, choices=STATUS, default="success")
    detail = models.TextField(verbose_name=_("detail"))

    class Meta:
        db_table = "bank_transaction"


class TransanctionW(models.Model):
    TRANSACTION_STATE = (
        ("send", "send"),
        ("recieve", "recieve"),
    )
    STATUS = (
        ("SUCCESS", "success"),
        ("FAILED", "failed"),
    )
    transaction_from = models.ForeignKey(User, verbose_name=_("transaction from"), on_delete=models.CASCADE, related_name="t_from_w")
    transaction_type = models.CharField(verbose_name=_("transaction type"), max_length=10, choices=TRANSACTION_STATE, default="send")

    amount = models.BigIntegerField(verbose_name=_("amount"), default=100)
    fee = models.BigIntegerField(verbose_name=_("feee"), default=10)
    state = models.CharField(verbose_name=_("state"), max_length=10, choices=STATUS, default="success")
    detail = models.TextField(verbose_name=_("detail"))

    class Meta:
        db_table = "bank_transaction_w"
