lst_holiday = [
    {"f_day": "1/1", "title": "فروردین جشن نوروز/جشن سال نو", "holiday": True, "is_lunar": False},
    {"f_day": "1/2", "title": "عیدنوروز", "holiday": True, "is_lunar": False},
    {"f_day": "1/3", "title": "عیدنوروز", "holiday": True, "is_lunar": False},
    {"f_day": "1/4", "title": "عیدنوروز", "holiday": True, "is_lunar": False},
    {"f_day": "1/12", "title": "روز جمهوری اسلامی", "holiday": True, "is_lunar": False},
    {"f_day": "1/13", "title": " جشن سیزده به در", "holiday": True, "is_lunar": False},
    {"f_day": "3/14", "title": "رحلت حضرت امام خمینی", "holiday": True, "is_lunar": False},
    {"f_day": "3/14", "title": "قیام 15 خرداد", "holiday": True, "is_lunar": False},
    {"f_day": "11/22", "title": "پیروزی انقلاب اسلامی", "holiday": True, "is_lunar": False},
    {"f_day": "12/29", "title": "پیروزی انقلاب اسلامی", "holiday": True, "is_lunar": False},
    {"f_day": "9/19", "title": "ضربت خوردن حضرت علی علیه السلام", "holiday": False, "is_lunar": True},
    {"f_day": "9/21", "title": "شهادت حضرت علی علیه السلام", "holiday": True, "is_lunar": True},
    {"f_day": "10/2", "title": "عید سعید فطر", "holiday": True, "is_lunar": True},
    {"f_day": "10/3", "title": "تعطیل به مناسبت عید سعید فطر", "holiday": True, "is_lunar": True},
    {"f_day": "10/26", "title": "شهادت امام جعفر صادق علیه السلام", "holiday": True, "is_lunar": True},
    {"f_day": "12/10", "title": "عید سعید قربان", "holiday": True, "is_lunar": True},
    {"f_day": "12/18", "title": "عید سعید غدیر خم", "holiday": True, "is_lunar": True},
    {"f_day": "1/9", "title": "تاسوعای حسینی", "holiday": True, "is_lunar": True},
    {"f_day": "1/10", "title": "عاشورای حسینی", "holiday": True, "is_lunar": True},
    {"f_day": "2/20", "title": "اربعین حسینی", "holiday": True, "is_lunar": True},
    {"f_day": "2/28", "title": "رحلت رسول اکرم؛شهادت امام حسن مجتبی علیه السلام", "holiday": True, "is_lunar": True},
    {"f_day": "2/30", "title": "شهادت امام رضا علیه السلام", "holiday": True, "is_lunar": True},
    {"f_day": "3/9", "title": "شهادت امام حسن عسکری علیه السلام", "holiday": True, "is_lunar": True},
    {"f_day": "3/17", "title": "میلاد رسول اکرم و امام جعفر صادق علیه السلام", "holiday": True, "is_lunar": True},
    {"f_day": "6/3", "title": "شهادت حضرت فاطمه زهرا سلام الله علیها", "holiday": True, "is_lunar": True},
    {"f_day": "7/13", "title": "ولادت امام علی علیه السلام و روز پدر", "holiday": True, "is_lunar": True},
    {"f_day": "7/27", "title": "مبعث رسول اکرم", "holiday": True, "is_lunar": True},
]

