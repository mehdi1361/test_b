from django.http import JsonResponse
from django.utils.deprecation import MiddlewareMixin
from communicate.auth import is_app_access, user_info


class ServiceMiddleware(MiddlewareMixin):
    def __init__(self, get_response):
        super().__init__(get_response)
        self.get_response = get_response

    def __call__(self, request):
        if not is_app_access():
            return JsonResponse({'message': 'app authorization failed'}, status=403)

        if request.path != '/api/v1/users/login/':
            if "Token" not in request.headers.keys():
                return JsonResponse({'message': f'cant find  user data header {request.headers}'}, status=404)

            result, err = user_info(request.headers["token"])
            if err is not None:
                return JsonResponse({'message': 'raise a problem please login again'}, status=401)

            if "message" in result.keys() and result["message"] == "user not found":
                return JsonResponse({'message': 'raise a problem pplease login again'}, status=401)

            request.service_user = result

        return self.get_response(request)


